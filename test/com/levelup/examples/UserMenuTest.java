package com.levelup.examples;

import com.levelup.examples.AddressBook;
import com.levelup.examples.SortMethod;
import com.levelup.examples.UIInteractions;
import com.levelup.examples.UserMenu;
import com.levelup.examples.UserMenuImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class UserMenuTest {

    @Mock
    public AddressBook addressBook;

    @Mock
    public UIInteractions uiInteractions;

    @InjectMocks
    public UserMenuImpl userMenu;


    @Test
    public void testMenuSort() {
        userMenu.act(UserMenu.MenuItems.SORT_BY_FIRSTNAME);
        verify(addressBook, times(1)).sortUsersBy(eq(SortMethod.BY_FIRSTNAME));

        userMenu.act(UserMenu.MenuItems.SORT_BY_LASTNAME);
        verify(addressBook, times(1)).sortUsersBy(eq(SortMethod.BY_LASTNAME));

        userMenu.act(UserMenu.MenuItems.SORT_BY_PHONE);
        verify(addressBook, times(1)).sortUsersBy(eq(SortMethod.BY_PHONE));

        when(uiInteractions.inputString(anyString())).thenReturn("aaaa");

        userMenu.act(UserMenu.MenuItems.FILTER_BY_NAME);
        verify(addressBook, times(1)).filterByName(eq("aaaa"), eq("aaaa"));
    }


}
