package com.levelup.examples;

import java.util.Scanner;

public class UIInteractions {

    public String inputString(String text) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(text);
        return scanner.next();
    }
}
